package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {
	
	@Autowired
	private FileManager fileManager;
	
	private String path = "portal_it_test/documents/";

	@GetMapping("/")
    public String index(Model model) {
		try {
			List<String> list = fileManager.getNodeList(path);
			model.addAttribute("files", list);
		} catch (Exception e) {
			model.addAttribute("files", new ArrayList<String>());
		}
        return "index";
    }
	
	@PostMapping("/")
    public String handleFileUpload(@RequestParam("file") MultipartFile file, @RequestParam("reference") String reference,
            RedirectAttributes redirectAttributes) {
		if (file.getOriginalFilename().isEmpty())
			redirectAttributes.addFlashAttribute("message", "You must select a file first.");
		else 
			try {
				fileManager.write(file.getInputStream(), reference, file.getOriginalFilename());
				redirectAttributes.addFlashAttribute("message", "You successfully uploaded " + file.getOriginalFilename() + "! ");
			} catch (Exception e) {
				redirectAttributes.addFlashAttribute("message", "An error has occurred.");
			} 
		
        return "redirect:/";
    }
	
	@GetMapping("/files/{filename:.+}")
	public void getFile(@PathVariable String filename, HttpServletResponse response) {
		String fileTitle = filename.substring(filename.lastIndexOf("-") + 1);
		try {
			if(fileManager.exists(filename.replaceAll("-", "/"))) {
				response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", fileTitle));
				InputStream is = fileManager.read(filename.replaceAll("-", "/"));
				FileCopyUtils.copy(is, response.getOutputStream());
			}
			else {
				System.out.println("The file " + filename + " not found!");
			}
		} catch (Exception e) { e.printStackTrace();}
	}
	
	@GetMapping("/delete/files/{filename:.+}")
	public String deleteFile(@PathVariable String filename) {
		try {
			fileManager.delete(filename.replaceAll("-", "/"));
		} catch (Exception e) { }
		return "redirect:/";
	}
}
