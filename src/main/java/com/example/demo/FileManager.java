package com.example.demo;

import java.io.InputStream;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;

import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.rmi.remote.RemoteRepository;
import org.apache.jackrabbit.rmi.server.ServerAdapterFactory;
import org.springframework.stereotype.Service;

@Service
public class FileManager {
	
	//o tamanho maximo do ficheiro permitido esta definido no application.properties
	
	private static final String BASE_PATH = "portal_it_test/documents/";
	private Session session;
	
	private RemoteRepository remoteRepository;
	
	public FileManager() throws RemoteException, RepositoryException {
		//start local repository session
		Repository repository = JcrUtils.getRepository();
		
		SimpleCredentials credentials = new SimpleCredentials("admin", "admin".toCharArray());
		this.session = repository.login(credentials);
        
        // Start the RMI registry
		Registry reg = LocateRegistry.createRegistry(1100);
		
        // Bind the repository reference to the registry
        ServerAdapterFactory factory = new ServerAdapterFactory();
        this.remoteRepository = factory.getRemoteRepository(repository);
        reg.rebind("jackrabbit", this.remoteRepository);
	}
	
	public List<String> getNodeList(String path) throws Exception {
		List<String> list = new ArrayList<String>();
		
    	Node rootNode = session.getRootNode();
    	Node dirNode = JcrUtils.getNodeIfExists(rootNode, path);
    	if (dirNode != null) {
    		Iterable<Node> nodes = JcrUtils.getChildNodes(dirNode);
    		for (Node node : nodes) {
    			Iterable<Node> nodesChild = JcrUtils.getChildNodes(node);
    			for (Node node2 : nodesChild) {
    				list.add(node.getName() + "-" + node2.getName());
				}
    		}
    	}
    	return list;
	}
	
	public String write(InputStream inputStream, String targetPath, String fileName) throws RepositoryException {
		String newFilePath = targetPath + "/" + fileName;
		
		Node rootNode = session.getRootNode(); 
		Node fileNode = JcrUtils.getOrCreateByPath(rootNode, BASE_PATH + targetPath + "/" + fileName, false, null, "nt:file", false);
    	Node contentNode = fileNode.addNode ("jcr:content", "nt:resource");
    	
    	Binary binary = session.getValueFactory().createBinary(inputStream);
        contentNode.setProperty("jcr:data", binary);
    	
        this.session.save();
        return newFilePath;
	}
	
	public InputStream read(String filePath) throws RepositoryException {
		Node rootNode = session.getRootNode(); 
    	Node fileNode = rootNode.getNode(BASE_PATH + filePath);
		InputStream is = JcrUtils.readFile(fileNode);
		return is;
	}

	public void delete(String filePath) throws RepositoryException {
		Node rootNode = session.getRootNode(); 
    	Node fileNode = rootNode.getNode(BASE_PATH + filePath);
    	
    	this.session.removeItem(fileNode.getPath());
    	this.session.save();
	}

	public boolean exists(String filePath) throws RepositoryException {
		Node rootNode = session.getRootNode();
		Node fileNode = JcrUtils.getNodeIfExists(rootNode, BASE_PATH + filePath);
		
		if(fileNode == null)
			return false;
		return true;
	}
	
	@PreDestroy
    public void destroy(){
		this.session.logout(); 
    }
}
